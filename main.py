from tokenize import generate_tokens, NUMBER, STRING, NAME
from openpyxl import Workbook, load_workbook
from io import BytesIO
from shutil import copy2
from os import path, listdir
import StringIO

def checkFileExists(filename):
	if path.isfile(filename):
		return filename
	else:
		printError("Could not find " + filename + " in current directory.")

def getMtTitle():
	for f in listdir("."):
		if f.endswith(".mt0"):
			mtTitle = path.splitext(f)[0]
			print "\nLocated " + f + ".\n"
			return mtTitle
	else:
		printError("Could not find mt0 file in current directory.")
			
def printError(message):
	print "\nError: " + message + "\n"
	quit()

def saveResults(mtTitle, results):
	wb = Workbook()
	ws = wb.active
	r = 1
	for key, value in results.iteritems():
		cellName = ws.cell(row = r, column = 1)
		cellValue = ws.cell(row = r, column = 2)
		cellName.value = key
		cellValue.value = value
		r += 1
	wb.save(mtTitle + ".xlsx")

def insertResults(filename, results):
	insertedResults = {} # stores values that were inserted
	wb = load_workbook(filename=filename, data_only=True) # the excel file located at filename
	sheet_names = wb.get_sheet_names() # the sheet names in the spreadsheet
	for sheet in sheet_names:
		numAdded = 0 # keeps track of the number of values added to a given sheet
		ws = wb[sheet] # a given sheet from the workbook
		for row in ws.rows:
			for cell in row:
				key = cell.value
				if key in results: # if the value exists in the dictionary
					cell.value = results[key] # replace the value's name with its number value
					insertedResults[key] = results[key] # add the key, value mapping to the dictionary
					numAdded += 1

		print "Inserted " + str(numAdded) + " values in " + sheet + "..."

	print "Inserted " + str(len(insertedResults)) + " unique values in total."

	wb.save(filename) # save the file

def parseCorner(filename, suffix):
	corner_file = open(filename, 'r')
	results_names = []
	results_values = []
	multiplier = 1
	for i, line in enumerate(corner_file):
		if (i >= 2):
			g = generate_tokens(StringIO.StringIO(line).readline)
			for toktype, tokval, _, _, _ in g:
				if (toktype == 51 and tokval == '-'):
					multiplier = -1
				if (toktype == NUMBER):
					results_values.append(float(tokval) * multiplier * 1000)
					multiplier = 1
				if (toktype == NAME):
					results_names.append(tokval + "_" + suffix)
					multiplier = 1
	if (len(results_names) != len(results_values)):
		printError("Number of values does not equal the number of value names.")	
	results = {}
	for i in xrange(len(results_names)):
		results[results_names[i]] = results_values[i]
	return results

def main():

	# Fill in these parameters
	mtTitle = getMtTitle()
	cornerTitle = checkFileExists("corners_list") 
	templateFile = checkFileExists("template.xlsx") 
	outputFile = "output.xlsx"
	suffix = "mt"

	# Iterate through corners in corner list
	corners_list = open(cornerTitle, 'r')
	results = {}
	i = 0
	for corner in corners_list:
		extension = suffix + str(i)
		cornername = mtTitle + '.' + extension
		if (checkFileExists(cornername)):
			print "Corner " + str(i) + ":\t'" + corner[0:40] + "'"
			print "Suffix: \t" + extension
			results.update(parseCorner(cornername, extension))
			saveResults(mtTitle, results)
			i += 1

	print "\nFound " + str(len(results)) + " values in " + str(i) + " corners."
	print "The values are stored in " + mtTitle + ".xlsx.\n"

	print "Reading template from " + templateFile + ", inserting values in " + outputFile + "."
	if (path.isfile(outputFile)):
		print outputFile + " already exists, and will be overwritten."
	user_input = raw_input("Proceed? (Y or N)\n")

	if (user_input.lower() == 'y'):
		# if proceeding
		copy2(templateFile, outputFile) # create the output file
		insertResults(outputFile, results) # insert the values in the output file
	else:
		quit()

main()
