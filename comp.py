import sys
from openpyxl import Workbook, load_workbook
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font
from io import BytesIO
from shutil import copy2
from os import path, listdir
import StringIO

def isDecimal(s):
	try:
		x = float(s)
		return True
	except ValueError:
		return False
	except TypeError:
		return False

def printError(message):
	print "\nError: " + message + "\n"
	quit()


def getResults(wb, sheet_name):
	# results is a dictionary between Tuple(Row, Column) and Float
	results = {}
	ws = wb[sheet_name]
	for row in ws.rows:
		for cell in row:
			if (isDecimal(cell.value)):
				coord = (cell.row, cell.column)
				results[coord] = float(cell.value)
	return results
	

def generateDiff(sheet_name, tol, results1, results2):
	results_diff = {}
	for key in results1.keys():
		val1 = results1[key]
		val2 = results2[key]
		realTol = val1 * tol
		diff = abs(val1 - val2)
		if (diff > realTol): results_diff[key] = diff
	print "Found " + str(len(results1.keys())) + " values in " + sheet_name + "."
	print str(len(results_diff)) + " value(s) in your second spreadsheet exceeded the tolerance." 
	return results_diff

def compareSheets(tol, file1, file2):
	wb1 = load_workbook(filename=file1, data_only=True)
	wb2 = load_workbook(filename=file2, data_only=True)
	sheet_names1 = wb1.get_sheet_names()
	sheet_names2 = wb2.get_sheet_names()
	if (sheet_names1 != sheet_names2):
		printError("The two sheets you provided do not have the same sheet names.")
	else:
		for sheet in sheet_names1:
			results1 = getResults(wb1, sheet)
			results2 = getResults(wb2, sheet)
			keys_diff = set(results1.keys()) - set(results2.keys())
			if (len(keys_diff) > 0):
				printError("The spreadsheets you provided are not comparable. We expect two spreadsheets as input, where the only difference between the two are the numeric values.")
			else:
				results_diff = generateDiff(sheet, tol, results1, results2)
				for (r, c) in results_diff.keys():
					cellname = str(c) + str(r)
					cell = (wb2[sheet])[cellname]
					cell.fill = redFill
		print ("\nView the diff in diff.xlsx.")
		wb2.save(filename="diff.xlsx")

def main(): 
	numArgs = len(sys.argv)
	if (numArgs < 3):
		printError("Expects two arguments: first, the reference file and then the other file.")
	file1 = path.basename(sys.argv[1])
	file2 = path.basename(sys.argv[2])
	print "Your reference file is: " + file1 + "."
	print "Your other file is: " + file2 + "."
	user_input = raw_input("Enter the tolerance, in percent. \n")
	if (isDecimal(user_input)):
		compareSheets(float(user_input) / 100, file1, file2)
	else:
		printError("That's not a valid tolerance.")

redFill = PatternFill(start_color='FFCCFF', end_color='FFCCFF', fill_type='solid')
main()


